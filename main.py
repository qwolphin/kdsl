from collections import defaultdict
from itertools import chain
from pathlib import Path

import astor
import json

from rendering.apischema import api_definitions, is_resource, gvk_from_spec
from rendering.parse import parse_resource_definition
from rendering.objects.other import DomainVersionName
from rendering.objects.definitions import K8sDefinition
from rendering.settings import OUTPUT_DIR, PACKAGE_NAME, DOCS_DATA_DIR
from rendering.utils import render_module


resource_objects = set()

for rdns, spec in api_definitions.items():
    dvn = DomainVersionName.from_rdns(rdns)

    if not is_resource(dvn):
        continue

    gvk = gvk_from_spec(spec)

    resource_objects.add(parse_resource_definition(dvn=dvn, spec=spec, gvk=gvk))

entries = {
    *resource_objects,
    *chain(*(x.full_requirements.entries for x in resource_objects)),
}

# uniqueness check
buckets = defaultdict(set)

for entry in entries:
    buckets[entry.uniqness_check_bucket].add(entry)

for bucket, members in buckets.items():
    assert len(members) == len({x.dvn for x in members}), bucket

# rendering
modules = defaultdict(lambda: defaultdict(set))
for d in entries:
    modules[d.dvn.module][d.version_module_name].add(d)

# import IPython; IPython.embed()

if __name__ == "__main__":
    docs_data_path = Path(DOCS_DATA_DIR)
    package_path = Path(OUTPUT_DIR)
    (package_path / "__init__.py").touch()

    for group, group_modules in modules.items():
        group_folder = package_path / group
        group_folder.mkdir()
        (group_folder / "__init__.py").touch()

        group_data_folder = docs_data_path / group
        group_data_folder.mkdir()

        for version, entries in group_modules.items():
            main_file = group_folder / f"{version}.py"
            with main_file.open("w") as f:
                f.writelines(
                    astor.to_source(
                        render_module(entries),
                        pretty_source=lambda x: x,  # bug workaround
                    )
                )

            data_file = group_data_folder / f"{version}.json"
            with data_file.open("w") as f:
                json.dump(
                    [x.docs_data for x in entries if isinstance(x, K8sDefinition)],
                    f,
                    indent=2,
                )
