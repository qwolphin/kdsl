FROM python:3.10 as requirements

COPY ./requirements.txt /requirements.txt 

RUN pip3 install --no-cache-dir --upgrade -r /requirements.txt \
    && mkdir /openapi

WORKDIR /app

FROM requirements as full

COPY . .
