import ast
import attr

from ..settings import PACKAGE_NAME
from ..ast_utils import (
    mk_aa,
    mk_union_type,
    mk_seq_type,
    mk_mapping_type,
)
from .other import Requirements


def optional_ifs(converting_return):
    omit_if = ast.If(
        test=ast.Compare(
            left=ast.Name("value"),
            ops=[ast.Is()],
            comparators=[ast.Name("OMIT")],
        ),
        body=[ast.Return(ast.Name("OMIT"))],
        orelse=[converting_return],
    )

    return ast.If(
        test=ast.Compare(
            left=ast.Name("value"),
            ops=[ast.Is()],
            comparators=[ast.Name("None")],
        ),
        body=[ast.Return(ast.Name("None"))],
        orelse=[omit_if],
    )


@attr.s(kw_only=True, eq=True, hash=True, frozen=True)
class ConverterTemplate:
    dvn = attr.ib()

    def mk(self, is_required):
        raise NotImplementedError()


@attr.s(kw_only=True, eq=True, hash=True, frozen=True)
class Converter:
    dvn = attr.ib()
    is_required = attr.ib()

    @property
    def uniqness_check_bucket(self):
        return f"{self.name_prefix}_{self.converter_type_name}"

    @property
    def version_module_name(self):
        return f"{self.dvn.version}"

    @property
    def module_attrs(self):
        return [PACKAGE_NAME, self.dvn.module, self.version_module_name]

    @property
    def module_full_name(self):
        return ".".join(self.module_attrs)

    @property
    def full_path_attrs(self):
        return [*self.module_attrs, self.name]

    @property
    def full_requirements(self):
        requirments = Requirements(from_imports={"typing": {"Union"}})

        if self.is_required:
            return requirments

        return requirments + Requirements(
            from_imports={
                "typing": {"Literal"},
                f"{PACKAGE_NAME}.bases": {"OmitEnum", "OMIT"},
            }
        )

    converter_type_name = ...

    @property
    def name_prefix(self):
        return "required" if self.is_required else "optional"

    @property
    def name(self):
        return "_".join(
            [
                self.name_prefix,
                self.converter_type_name,
                self.dvn.name,
            ]
        )

    def wrap_annotation(self, ann):
        if self.is_required:
            return ann

        return mk_union_type(
            ann,
            ast.Name("OmitEnum"),
            ast.Constant(None),
        )


@attr.s(kw_only=True, eq=True, hash=True, frozen=True)
class SimpleConverterTemplate(ConverterTemplate):
    def mk(self, is_required):
        return SimpleConverter(
            dvn=self.dvn,
            is_required=is_required,
        )


@attr.s(kw_only=True, eq=True, hash=True, frozen=True)
class SimpleConverter(Converter):
    converter_type_name = "converter"

    @property
    def parent_converter(self):
        return None

    @property
    def expressions(self):
        dvn = self.dvn

        arguments = ast.arguments(
            args=[
                ast.arg(
                    arg="value",
                    annotation=self.wrap_annotation(
                        ast.Name(dvn.union_name),
                    ),
                )
            ],
            defaults=[],
            vararg=None,
            kwarg=None,
        )

        body = [
            ast.Return(
                ast.IfExp(
                    test=ast.Call(
                        func=ast.Name("isinstance"),
                        args=[ast.Name("value"), ast.Name("dict")],
                        keywords=[],
                    ),
                    body=ast.Call(
                        func=ast.Name(dvn.name),
                        args=[],
                        keywords=[ast.keyword(arg=None, value=ast.Name("value"))],
                    ),
                    orelse=ast.Name("value"),
                )
            ),
        ]

        return [
            ast.FunctionDef(
                name=self.name,
                decorator_list=[],
                args=arguments,
                keywords=[],
                returns=self.wrap_annotation(
                    ast.Name(dvn.name),
                ),
                body=body,
            )
        ]


@attr.s(kw_only=True, eq=True, hash=True, frozen=True)
class ListConverterTemplate(ConverterTemplate):
    def mk(self, is_required):
        return ListConverter(
            dvn=self.dvn,
            is_required=is_required,
        )


@attr.s(kw_only=True, eq=True, hash=True, frozen=True)
class ListConverter(Converter):
    converter_type_name = "list_converter"

    @property
    def parent_converter(self):
        return SimpleConverter(dvn=self.dvn, is_required=True)

    @property
    def full_requirements(self):
        pc = self.parent_converter
        return super().full_requirements + Requirements(
            from_imports={"typing": {"Sequence"}}, entries={pc} if pc else {}
        )

    @property
    def expressions(self):
        dvn = self.dvn

        arg_annotation = mk_seq_type(ast.Name(dvn.union_name))
        ret_annotation = mk_seq_type(ast.Name(dvn.name))

        if not self.is_required:
            arg_annotation = mk_union_type(arg_annotation)

        arguments = ast.arguments(
            args=[
                ast.arg(
                    arg="value",
                    annotation=self.wrap_annotation(
                        mk_seq_type(ast.Name(dvn.union_name)),
                    ),
                )
            ],
            defaults=[],
            vararg=None,
            kwarg=None,
        )

        converting_return = ast.Return(
            ast.ListComp(
                elt=ast.Call(
                    func=ast.Name(self.parent_converter.name),
                    args=[ast.Name("x")],
                    keywords=[],
                ),
                generators=[
                    ast.comprehension(
                        target=ast.Name("x"),
                        iter=ast.Name("value"),
                        ifs=[],
                        is_async=0,
                    ),
                ],
            )
        )

        body = [
            converting_return if self.is_required else optional_ifs(converting_return)
        ]
        return [
            ast.FunctionDef(
                name=self.name,
                decorator_list=[],
                args=arguments,
                keywords=[],
                returns=self.wrap_annotation(mk_seq_type(ast.Name(dvn.name))),
                body=body,
            )
        ]


@attr.s(kw_only=True, eq=True, hash=True, frozen=True)
class ListMappingConverterTemplate(ConverterTemplate):
    key_field = attr.ib()

    def mk(self, is_required):
        return ListMappingConverter(
            dvn=self.dvn,
            key_field=self.key_field,
            is_required=is_required,
        )


@attr.s(kw_only=True, eq=True, hash=True, frozen=True)
class ListMappingConverter(Converter):
    key_field = attr.ib()

    converter_type_name = "mlist_converter"

    @property
    def parent_converter(self):
        return SimpleConverter(dvn=self.dvn, is_required=True)

    @property
    def full_requirements(self):
        pc = self.parent_converter
        return super().full_requirements + Requirements(
            from_imports={"typing": {"Mapping"}}, entries={pc} if pc else {}
        )

    @property
    def expressions(self):
        dvn = self.dvn
        key_annotation = self.key_field.annotation
        arguments = ast.arguments(
            args=[
                ast.arg(
                    arg="value",
                    annotation=self.wrap_annotation(
                        mk_mapping_type(
                            key_annotation,
                            ast.Name(dvn.union_name),
                        )
                    ),
                )
            ],
            defaults=[],
            vararg=None,
            kwarg=None,
        )

        converting_return = ast.Return(
            ast.DictComp(
                key=ast.Name("k"),
                value=ast.Call(
                    func=ast.Name(self.parent_converter.name),
                    args=[ast.Name("v")],
                    keywords=[],
                ),
                generators=[
                    ast.comprehension(
                        target=ast.Tuple(elts=[ast.Name("k"), ast.Name("v")]),
                        iter=ast.Call(
                            func=mk_aa("value", "items"),
                            args=[],
                            keywords=[],
                        ),
                        ifs=[],
                        is_async=0,
                    ),
                ],
            )
        )

        body = [
            converting_return if self.is_required else optional_ifs(converting_return)
        ]
        return [
            ast.FunctionDef(
                name=self.name,
                decorator_list=[],
                args=arguments,
                keywords=[],
                returns=self.wrap_annotation(
                    mk_mapping_type(key_annotation, ast.Name(dvn.name))
                ),
                body=body,
            )
        ]
