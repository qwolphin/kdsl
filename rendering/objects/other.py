import attr
import ast
from ..settings import PACKAGE_NAME
from .. import constants


@attr.s(kw_only=True, eq=True, hash=True, frozen=True)
class GroupVersionKind:
    group = attr.ib()
    version = attr.ib()
    kind = attr.ib()

    @property
    def apiversion(self):
        group = self.group
        version = self.version

        if group == "":
            return version

        return f"{group}/{version}"


@attr.s(kw_only=True, eq=True, hash=True, frozen=True)
class DomainVersionName:
    domain = attr.ib()
    version = attr.ib()
    name = attr.ib()

    def renamed(self, new_name):
        return DomainVersionName(
            domain=self.domain,
            version=self.version,
            name=new_name,
        )

    @classmethod
    def from_rdns(cls, rdns):
        *domain_list, version, name = rdns.split(".")
        domain = ".".join(domain_list)

        return cls(domain=domain, version=version, name=name)

    @classmethod
    def from_pointer(cls, pointer):
        hash_symbol, definitions_name, rdns = pointer.split("/")

        assert hash_symbol == "#"
        assert definitions_name == "definitions"

        return cls.from_rdns(rdns)

    # COMMON
    @property
    def rdns(self):
        return ".".join([self.domain, self.version, self.name])

    @property
    def module(self):
        return constants.RDNS_DOMAIN_TO_MODULE[self.domain]

    @property
    def module_attrs(self):
        return [PACKAGE_NAME, self.module, self.version]

    @property
    def module_full_name(self):
        return ".".join(self.module_attrs)

    @property
    def full_path_attrs(self):
        return [*self.module_attrs, self.name]

    # TYPED DICT
    @property
    def typed_dict_name(self):
        return f"{self.name}TypedDict"

    @property
    def typed_dict_full_path_attrs(self):
        return [*self.module_attrs, self.typed_dict_name]

    # UNION
    @property
    def union_name(self):
        return f"{self.name}Union"

    @property
    def union_full_path_attrs(self):
        return [*self.module_attrs, self.union_name]


class hashabledict(dict):
    def __hash__(self):
        return hash(tuple(sorted(self.items())))


@attr.s(kw_only=True, eq=True, hash=True, frozen=True)
class Requirements:
    simple_imports = attr.ib(factory=frozenset, converter=frozenset)
    from_imports = attr.ib(
        factory=hashabledict,
        converter=lambda x: hashabledict({k: frozenset(v) for k, v in x.items()}),
    )
    entries = attr.ib(factory=frozenset, converter=frozenset)

    def _get_from_module(self, name):
        return self.from_imports.get(name, frozenset())

    def __add__(self, other):
        assert isinstance(other, Requirements)

        return Requirements(
            simple_imports=self.simple_imports | other.simple_imports,
            from_imports={
                key: self._get_from_module(key) | other._get_from_module(key)
                for key in frozenset(self.from_imports.keys())
                | frozenset(other.from_imports.keys())
            },
            entries=self.entries | other.entries,
        )

    @property
    def expressions(self):
        return [
            *(
                ast.Import(names=[ast.alias(name=name, asname=None)])
                for name in self.simple_imports
            ),
            *(
                ast.ImportFrom(
                    module=module,
                    names=[ast.alias(name=name, asname=None) for name in names],
                    level=0,
                )
                for module, names in self.from_imports.items()
            ),
        ]
