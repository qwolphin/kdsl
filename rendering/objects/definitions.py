import ast
import attr
from ..ast_utils import (
    mk_attr_s,
    mk_classvar_type,
    mk_typed_dict,
    mk_union_type,
)
from ..settings import PACKAGE_NAME

from .other import Requirements
from .converters import SimpleConverter


@attr.s(kw_only=True, eq=True, hash=True, frozen=True)
class K8sDefinition:
    dvn = attr.ib()

    description = attr.ib()
    fields = attr.ib(converter=frozenset)

    resource_assignments = []
    base_class = "K8sObject"
    is_resource = False

    uniqness_check_bucket = "definition"

    @property
    def docs_data(self):
        return dict(
            type="definition",
            **self.dvn.__dict__,
            description=self.description,
            fields=[x.docs_data for x in self.fields],
        )

    @property
    def version_module_name(self):
        return self.dvn.version

    @property
    def sorted_fields(self):
        return sorted(self.fields, key=lambda x: x.python_name)

    @property
    def required_fields(self):
        return [x for x in self.sorted_fields if x.is_required]

    @property
    def optional_fields(self):
        return [x for x in self.sorted_fields if not x.is_required]

    @property
    def full_requirements(self):
        return (
            Requirements(
                simple_imports={"attr"},
                from_imports={
                    "typing": {"ClassVar", "TypedDict"},
                    f"{PACKAGE_NAME}.bases": {self.base_class, "OMIT", "OmitEnum"},
                },
                entries={
                    SimpleConverter(dvn=self.dvn, is_required=True),
                    SimpleConverter(dvn=self.dvn, is_required=False),
                }
                if not self.is_resource
                else {},
            )
            + sum((x.full_requirements for x in self.fields), start=Requirements())
        )

    @property
    def typed_dicts_ast(self):
        name = self.dvn.name
        dict_name = self.dvn.typed_dict_name

        if self.optional_fields and self.required_fields:
            optional_dict_name = f"{name}OptionalTypedDict"
            return [
                mk_typed_dict(
                    name=optional_dict_name,
                    fields=self.optional_fields,
                    total=False,
                ),
                mk_typed_dict(
                    name=dict_name,
                    fields=self.required_fields,
                    total=True,
                    base=optional_dict_name,
                ),
            ]

        if self.optional_fields:
            return [
                mk_typed_dict(
                    name=dict_name,
                    fields=self.optional_fields,
                    total=False,
                )
            ]

        if self.required_fields:
            return [
                mk_typed_dict(
                    name=dict_name,
                    fields=self.required_fields,
                    total=True,
                )
            ]

        assert False

    @property
    def class_ast(self):
        body = [
            *self.resource_assignments,
            *(x.class_assignment for x in self.required_fields),
            *(x.class_assignment for x in self.optional_fields),
        ]

        return ast.ClassDef(
            decorator_list=[mk_attr_s(kw_only=ast.Constant(True))],
            bases=[ast.Name(self.base_class)],
            keywords=[],
            name=self.dvn.name,
            body=body,
        )

    @property
    def expressions(self):
        return [
            self.class_ast,
            *self.typed_dicts_ast,
            ast.Assign(
                targets=[ast.Name(self.dvn.union_name)],
                value=mk_union_type(
                    ast.Name(self.dvn.name),
                    ast.Name(self.dvn.typed_dict_name),
                ),
            ),
        ]


@attr.s(kw_only=True, eq=True, hash=True, frozen=True)
class K8sResource(K8sDefinition):
    gvk = attr.ib()
    is_resource = True
    base_class = "K8sResource"

    def __attrs_post_init__(self):
        assert self.dvn.name == self.gvk.kind
        assert self.dvn.version == self.gvk.version

    @property
    def docs_data(self):
        return {
            **super().docs_data,
            "apiVersion": self.gvk.apiversion,
            "type": "resource",
        }

    @property
    def resource_assignments(self):
        gvk = self.gvk
        return [
            ast.AnnAssign(
                target=ast.Name("apiVersion"),
                annotation=mk_classvar_type(ast.Name("str")),
                value=ast.Constant(gvk.apiversion),
                simple=True,
            ),
            ast.AnnAssign(
                target=ast.Name("kind"),
                annotation=mk_classvar_type(ast.Name("str")),
                value=ast.Constant(gvk.kind),
                simple=True,
            ),
        ]

    @property
    def expressions(self):
        return [
            self.class_ast,
        ]
