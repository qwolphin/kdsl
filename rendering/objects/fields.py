import ast
import astor
import attr

from ..ast_utils import mk_attr_ib, mk_union_type, mk_aa
from ..settings import PACKAGE_NAME
from .other import Requirements


@attr.s(kw_only=True, eq=True, hash=True, frozen=True)
class K8sFieldTemplate:
    annotation = attr.ib(eq=False)  # annotation AST
    requirements = attr.ib(factory=Requirements)
    converter_template = attr.ib(default=None)
    is_embedable = attr.ib(default=False)
    mlist_key = attr.ib(default=None)
    dvn = attr.ib(default=None)

    @property
    def full_requirements(self):
        return self.requirements

    def mk_unbound(self, description):
        assert isinstance(self, K8sFieldTemplate)

        return K8sUnboundField(
            annotation=self.annotation,
            requirements=self.requirements,
            converter_template=self.converter_template,
            mlist_key=self.mlist_key,
            is_embedable=self.is_embedable,
            description=description,
        )


@attr.s(kw_only=True, eq=True, hash=True, frozen=True)
class K8sUnboundField(K8sFieldTemplate):
    description = attr.ib()  # nullable

    def mk_bound(self, is_required, yaml_name, python_name, parent_dvn):
        assert isinstance(self, K8sUnboundField)
        return K8sBoundField(
            annotation=self.annotation,
            requirements=self.requirements,
            converter_template=self.converter_template,
            mlist_key=self.mlist_key,
            is_embedable=self.is_embedable,
            description=self.description,
            is_required=is_required,
            yaml_name=yaml_name,
            python_name=python_name,
            parent_dvn=parent_dvn,
        )


@attr.s(kw_only=True, eq=True, hash=True, frozen=True)
class K8sBoundField(K8sUnboundField):
    is_required = attr.ib()
    yaml_name = attr.ib()
    python_name = attr.ib()
    parent_dvn = attr.ib()

    @property
    def docs_data(self):
        return dict(
            is_required=self.is_required,
            python_name=self.python_name,
            description=self.description,
            yaml_name=self.yaml_name if self.yaml_name != self.python_name else None,
            annotation=self.annotation_as_string,
        )

    @property
    def annotation_as_string(self):
        return astor.to_source(ast.Module([self.annotation]),)[
            :-1
        ]  # remove endl

    @property
    def converter(self):
        if self.converter_template is None:
            return None

        return self.converter_template.mk(is_required=self.is_required)

    @property
    def full_annotation(self):
        if self.is_required:
            return self.annotation

        return mk_union_type(ast.Constant(None), ast.Name("OmitEnum"), self.annotation)

    @property
    def full_requirements(self):
        converter = self.converter
        requirements = self.requirements

        if converter:
            entries = {converter}

            if converter.parent_converter:
                entries |= {converter.parent_converter}

            requirements += Requirements(
                simple_imports={
                    converter.module_full_name,
                },
                entries=entries,
            )

        if self.is_required:
            return requirements
        else:
            return requirements + Requirements(
                from_imports={
                    "typing": {"Optional"},
                    f"{PACKAGE_NAME}.bases": {"OMIT", "OmitEnum"},
                }
            )

    @property
    def attrib(self):
        if self.mlist_key:
            mlist_keys = [ast.Constant("mlist_key")]
            mlist_values = [ast.Constant(self.mlist_key)]
        else:
            mlist_keys = []
            mlist_values = []

        params = dict(
            metadata=ast.Dict(
                keys=[ast.Constant("yaml_name"), *mlist_keys],
                values=[ast.Constant(self.yaml_name), *mlist_values],
            ),
        )

        c = self.converter
        if c is not None:
            if self.parent_dvn.module_attrs == c.dvn.module_attrs:
                params["converter"] = ast.Name(c.name)
            else:
                params["converter"] = mk_aa(*c.full_path_attrs)

        if not self.is_required:
            params["default"] = ast.Name("OMIT")

        return mk_attr_ib(**params)

    @property
    def class_assignment(self):
        return ast.AnnAssign(
            target=ast.Name(self.python_name),
            annotation=self.full_annotation,
            value=self.attrib,
            simple=True,
        )

    @property
    def typed_dict_assignment(self):
        return ast.AnnAssign(
            target=ast.Name(self.python_name),
            annotation=self.annotation,
            value=None,
            simple=True,
        )
