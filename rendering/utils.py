import ast
from itertools import chain
from .objects.other import Requirements
from .objects.converters import Converter


def render_module(entries):
    requirements = sum((x.full_requirements for x in entries), start=Requirements())

    body = [
        ast.ImportFrom(
            module="__future__",
            names=[ast.alias(name="annotations", asname=None)],
            level=0,
        ),
        *requirements.expressions,
    ]

    entries = sorted(
        entries,
        key=lambda x: (
            not isinstance(x, Converter),
            x.__class__.__name__,
            getattr(x, "name", x.dvn.name),
        ),
    )
    body += chain(*(x.expressions for x in entries))
    return ast.Module(body)
