import json
import yaml
import jsonpatch

from .settings import OPENAPI_PATH, JSONPATCH_PATH
from .constants import GVK_FIELD, SPECIAL_DEFINITIONS
from .objects.other import GroupVersionKind


def gvk_from_spec(spec):
    try:
        (gvk_dict,) = spec[GVK_FIELD]
    except (KeyError, ValueError):
        raise ValueError("Spec has no gvk")

    return GroupVersionKind(**gvk_dict)


def is_resource(dvn):
    rdns = dvn.rdns

    if rdns in SPECIAL_DEFINITIONS:
        return False

    spec = api_definitions[rdns]

    try:
        gvk = gvk_from_spec(spec)
    except ValueError:
        return False

    return gvk in createables


def is_namespaced(dvn):
    assert is_resource(dvn), dvn

    spec = api_definitions[dvn.rdns]
    gvk = gvk_from_spec(spec)

    return gvk in namespaced_createables


with open(OPENAPI_PATH, "r") as f:
    apischema = json.load(f)

with open(JSONPATCH_PATH, "r") as f:
    patch = yaml.safe_load(f)


j_patch = jsonpatch.JsonPatch(patch)
apischema = j_patch.apply(apischema)
api_definitions = apischema["definitions"]
paths = apischema["paths"]


createables = set()
namespaced_createables = set()


post_operations = {k: v["post"] for k, v in paths.items() if "post" in v}
for path, spec in post_operations.items():
    gvk = GroupVersionKind(**spec[GVK_FIELD])

    if (
        spec["operationId"].startswith("create")
        and gvk.kind
        != "DeploymentRollback"  # it's deprecated and is not a normal resource
    ):
        createables.add(gvk)

        if "namespaces/{namespace}" in path:
            namespaced_createables.add(gvk)
