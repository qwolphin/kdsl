import os

from envparse import env

OPENAPI_PATH = env.str("OPENAPI_PATH")
assert os.path.isfile(OPENAPI_PATH), OPENAPI_PATH

JSONPATCH_PATH = env.str("JSONPATCH_PATH")
assert os.path.isfile(JSONPATCH_PATH), JSONPATCH_PATH

OUTPUT_DIR = env.str("OUTPUT_DIR")
assert os.path.isdir(OUTPUT_DIR), OUTPUT_DIR

DOCS_DATA_DIR = env.str("DOCS_DATA_DIR")
assert os.path.isdir(DOCS_DATA_DIR), DOCS_DATA_DIR

PACKAGE_NAME = env.str("PACKAGE_NAME")
assert PACKAGE_NAME.isidentifier(), PACKAGE_NAME
