import warnings

from ast import Name
from keyword import iskeyword

from .apischema import api_definitions, is_resource
from .ast_utils import (
    mk_mapping_type,
    mk_seq_type,
    mk_union_type,
    mk_literal_type,
    mk_aa,
)
from .constants import COMMON_FIELDS, SPECIAL_DEFINITIONS, any_field_template
from .objects.other import Requirements, DomainVersionName
from .objects.fields import K8sUnboundField
from .objects.definitions import K8sDefinition, K8sResource
from .objects.converters import (
    SimpleConverterTemplate,
    ListConverterTemplate,
    ListMappingConverterTemplate,
)


def parse_definition_fields(dvn, properties, required_fields):
    fields = set()

    for p_name, p_spec in properties.items():
        assert p_name.isidentifier()

        if iskeyword(p_name):
            # FIXME possible python_name collisions
            python_name = f"{p_name}_"
        else:
            python_name = p_name

        unbound_field = parse_field(dvn, python_name, p_spec)

        fields.add(
            unbound_field.mk_bound(
                is_required=p_name in required_fields,
                yaml_name=p_name,
                python_name=python_name,
                parent_dvn=dvn,
            )
        )

    return frozenset(fields)


def parse_simple_definition(dvn, spec):
    try:
        properties = spec["properties"]
    except:
        print(dvn)
        raise
    required = set(spec.get("required", []))
    fields = parse_definition_fields(dvn, properties, required)
    return K8sDefinition(
        dvn=dvn,
        fields=fields,
        description=spec.get("description", f"Kubernates API object: {dvn}"),
    )


def parse_resource_definition(dvn, gvk, spec):
    properties = spec["properties"]
    required = set(spec.get("required", []))

    assert "apiVersion" in properties
    assert "kind" in properties
    assert "metadata" in properties

    assert dvn.name == gvk.kind
    assert dvn.version == gvk.version

    properties = {
        k: v for k, v in properties.items() if k not in {"apiVersion", "kind"}
    }
    fields = parse_definition_fields(dvn, properties, required)

    return K8sResource(
        dvn=dvn,
        fields=fields,
        description=spec.get("description", f"Kubernates API object: {dvn}"),
        gvk=gvk,
    )


def parse_field(parent_dvn, field_name, spec):
    non_common_fields = set(spec.keys()) - COMMON_FIELDS
    description = spec.get("description", None)

    if "$ref" in spec:
        assert non_common_fields <= {"$ref"}, non_common_fields

        dvn = DomainVersionName.from_pointer(spec["$ref"])
        child_spec = api_definitions[dvn.rdns]

        try:
            field_template = SPECIAL_DEFINITIONS[dvn.rdns]
        except KeyError:
            pass
        else:
            return field_template.mk_unbound(description=description)

        if is_resource(dvn):
            print(f"Embeding {dvn}")
            dvn = dvn.renamed(f"Embedded{dvn.name}")

        inner_definition = parse_simple_definition(dvn, child_spec)

        if parent_dvn.module_attrs == dvn.module_attrs: # we are in the same module
            annotation = mk_aa(dvn.name)
            simple_imports = set()
        else:
            annotation = mk_aa(*dvn.full_path_attrs)
            simple_imports = {dvn.module_full_name}

        return K8sUnboundField(
            annotation=annotation,
            requirements=Requirements(
                simple_imports=simple_imports,
                from_imports={"typing": {"Union"}},
                entries={inner_definition},
            )
            + inner_definition.full_requirements,
            description=description,
            converter_template=SimpleConverterTemplate(dvn=dvn),
            dvn=dvn,
        )

    if "enum" in spec:
        return K8sUnboundField(
            annotation=mk_literal_type(*spec["enum"]),
            requirements=Requirements(from_imports={"typing": {"Literal"}}),
            description=description,
        )

    if "type" in spec:
        t = spec["type"]
    else:
        return K8sUnboundField(
            annotation=Name("Any"),
            requirements=Requirements(from_imports={"typing": {"Any"}}),
            description=description,
        )

    if t == "object":
        if "properties" in spec:
            assert False, "Untested branch"
            inner_name = "".join(parent_dvn.name, field_name[0].upper(), field_name[1:])
            inner_dvn = parent_dvn.renamed(inner_name)
            definition = parse_simple_definition(inner_dvn, spec)

            return K8sUnboundField(
                annotation=mk_union_type(
                    mk_aa(*dvn.full_path_attrs),
                    mk_aa(*dvn.typed_dict_full_path_attrs),
                ),
                requirements=Requirements(
                    simple_imports={dvn.module_full_name},
                    from_imports={"typing": {"Union"}},
                    entries={definition},
                ),
                description=description,
            )

        if "additionalProperties" in spec:
            assert non_common_fields <= {"additionalProperties"}, non_common_fields
            inner_field = parse_field(
                parent_dvn, "Mapping", spec["additionalProperties"]
            )
            # we don't have a converter for mappings that contain definitions
            assert not inner_field.full_requirements.entries
        else:
            inner_field = any_field_template

        return K8sUnboundField(
            annotation=mk_mapping_type(Name("str"), inner_field.annotation),
            requirements=Requirements(
                from_imports={"typing": {"Mapping"}},
            )
            + inner_field.full_requirements,
            description=description,
        )

    if t == "array":
        assert non_common_fields <= {"items"}, non_common_fields
        if "items" in spec:
            inner_spec = spec["items"]
            mapping_key = spec.get("x-kubernetes-patch-merge-key", None)

            if mapping_key:
                # we don't support inline objects here
                pointer = inner_spec["$ref"]
                dvn = DomainVersionName.from_pointer(pointer)
                item_spec = api_definitions[dvn.rdns]
                properties = item_spec["properties"]

                if len(properties) > 1:
                    key_spec = properties[mapping_key]
                    key_field = parse_field(parent_dvn, "MappingKey", key_spec)
                    value_dvn = dvn.renamed(f"{dvn.name}Item")
                    value_spec = dict(
                        item_spec,
                        properties={
                            k: v for k, v in properties.items() if k != mapping_key
                        },
                    )
                    value_definition = parse_simple_definition(value_dvn, value_spec)
                    assert key_field.is_embedable

                    return K8sUnboundField(
                        annotation=mk_mapping_type(
                            key_field.annotation, mk_aa(*value_dvn.full_path_attrs)
                        ),
                        requirements=Requirements(
                            simple_imports={
                                value_dvn.module_full_name,
                            },
                            from_imports={"typing": {"Mapping"}},
                            entries={value_definition},
                        )
                        + key_field.full_requirements
                        + value_definition.full_requirements,
                        converter_template=ListMappingConverterTemplate(
                            dvn=value_dvn, key_field=key_field
                        ),
                        description=description,
                        mlist_key=mapping_key,
                    )
                else:
                    warnings.warn(f"Empty spec: {parent_dvn} {field_name}")

            inner_field = parse_field(parent_dvn, "Array", inner_spec)
            if inner_field.dvn is not None:
                converter_template = ListConverterTemplate(dvn=inner_field.dvn)
            else:
                converter_template = None
        else:
            inner_field = any_field_template
            converter_template = None

        return K8sUnboundField(
            annotation=mk_seq_type(inner_field.annotation),
            requirements=Requirements(
                from_imports={"typing": {"Sequence"}},
            )
            + inner_field.full_requirements,
            converter_template=converter_template,
            description=description,
        )

    if t == "string":
        assert non_common_fields <= {"format"}, non_common_fields
        if "format" in spec:
            f = spec["format"]  # FIXME
            warnings.warn(f"Unhandled string format: {f}")

        return K8sUnboundField(
            annotation=Name("str"),
            description=description,
            is_embedable=True,
        )

    if t == "boolean":
        assert non_common_fields <= set(), non_common_fields
        return K8sUnboundField(
            annotation=Name("bool"),
            description=description,
        )

    if t == "integer":
        assert non_common_fields <= {"format"}, non_common_fields
        if "format" in spec:
            f = spec["format"]

        return K8sUnboundField(
            annotation=Name("int"),
            description=description,
            is_embedable=True,
        )

    if t == "number":
        assert non_common_fields <= {"format"}, non_common_fields
        if "format" in spec:
            f = spec["format"]

        return K8sUnboundField(
            annotation=Name("float"),
            description=description,
        )

    assert False
