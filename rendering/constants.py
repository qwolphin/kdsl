from ast import Name

from .ast_utils import mk_union_type, mk_mapping_type
from .objects.fields import K8sFieldTemplate
from .objects.other import Requirements

GVK_FIELD = "x-kubernetes-group-version-kind"

COMMON_FIELDS = {
    "x-kubernetes-map-type",
    "x-kubernetes-list-map-keys",
    "x-kubernetes-list-type",
    "x-kubernetes-patch-merge-key",
    "x-kubernetes-patch-strategy",
    "description",
    "type",
    "x-kubernetes-group-version-kind",
}


any_field_template = K8sFieldTemplate(
    annotation=Name("Any"),
    requirements=Requirements(from_imports={"typing": {"Any"}}),
)


mapping_field_template = K8sFieldTemplate(
    annotation=mk_mapping_type(Name("str"), Name("Any")),
    requirements=Requirements(from_imports={"typing": {"Any", "Mapping"}}),
)

string_field_template = K8sFieldTemplate(
    annotation=Name("str"),
    is_embedable=True,
)


RDNS_DOMAIN_TO_MODULE = {
    "io.k8s.api.core": "core",
    "io.k8s.apimachinery.pkg.apis.meta": "core",
    "io.k8s.api.admissionregistration": "admissionregistration",
    "io.k8s.apiextensions-apiserver.pkg.apis.apiextensions": "apiextensions",
    "io.k8s.kube-aggregator.pkg.apis.apiregistration": "apiregistration",
    "io.k8s.api.auditregistration": "auditregistration",
    "io.k8s.api.flowcontrol": "flowcontrol",
    "io.k8s.api.apps": "apps",
    "io.k8s.api.authentication": "authentication",
    "io.k8s.api.authorization": "authorization",
    "io.k8s.api.autoscaling": "autoscaling",
    "io.k8s.api.batch": "batch",
    "io.k8s.api.certificates": "certificates",
    "io.k8s.api.coordination": "coordination",
    "io.k8s.api.discovery": "discovery",
    "io.k8s.api.events": "events",
    "io.k8s.api.extensions": "extensions",
    "io.k8s.api.networking": "networking",
    "io.k8s.api.node": "node",
    "io.k8s.api.policy": "policy",
    "io.k8s.api.rbac": "rbac",
    "io.k8s.api.scheduling": "scheduling",
    "io.k8s.api.storage": "storage",
    "io.k8s.api.settings": "settings",
    "io.k8s.storage.snapshot": "storage_snapshot",
    "io.k8s.api.apiserverinternal": "apiserverinternal",
}

SPECIAL_DEFINITIONS = {
    "io.k8s.api.apiserverinternal.v1alpha1.StorageVersionSpec": any_field_template,
    "io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.CustomResourceSubresourceStatus": mapping_field_template,
    "io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSON": any_field_template,
    "io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSONSchemaProps": any_field_template,
    "io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSONSchemaPropsOrArray": any_field_template,
    "io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSONSchemaPropsOrBool": any_field_template,
    "io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSONSchemaPropsOrStringArray": any_field_template,
    "io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.CustomResourceSubresourceStatus": mapping_field_template,
    "io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSON": any_field_template,
    "io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaProps": any_field_template,
    "io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaPropsOrArray": any_field_template,
    "io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaPropsOrBool": any_field_template,
    "io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaPropsOrStringArray": any_field_template,
    "io.k8s.apimachinery.pkg.api.resource.Quantity": string_field_template,
    "io.k8s.apimachinery.pkg.apis.meta.v1.FieldsV1": mapping_field_template,
    "io.k8s.apimachinery.pkg.apis.meta.v1.Fields": mapping_field_template,
    "io.k8s.apimachinery.pkg.apis.meta.v1.MicroTime": string_field_template,
    "io.k8s.apimachinery.pkg.apis.meta.v1.Patch": mapping_field_template,
    "io.k8s.apimachinery.pkg.apis.meta.v1.Time": string_field_template,
    "io.k8s.apimachinery.pkg.runtime.RawExtension": mapping_field_template,
    "io.k8s.apimachinery.pkg.util.intstr.IntOrString": K8sFieldTemplate(
        annotation=mk_union_type(Name("int"), Name("str")),
        requirements=Requirements(from_imports={"typing": {"Union"}}),
    ),
}
