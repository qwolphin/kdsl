#!/usr/bin/env bash
set -e

export IMAGE_PULL_USER=meow
export IMAGE_PULL_TOKEN=miaou
export NAMESPACE=test
export BACKEND_IMAGE=backend_image
export PROXY_IMAGE=proxy_image
export FRONTEND_PROD_IMAGE=frontend_prod_image
export FRONTEND_DEV_IMAGE=frontend_dev_image
export DOMAIN=test.wcr.fi
export RECIPE=dev

cd complex
python3 main.py
