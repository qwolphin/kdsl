import values

from kdsl.extra import RegistryAuth, RegistryAuthSecret
from kdsl.core.v1 import Namespace, ObjectMeta
from envparse import env  # type: ignore

namespace = Namespace(
    metadata=ObjectMeta(name=values.NAMESPACE),
)

reg_secret_name = "reg-secret"

reg_secret = RegistryAuthSecret(
    metadata=ObjectMeta(
        name=reg_secret_name,
        namespace=values.NAMESPACE,
    ),
    registries={
        "registry.gitlab.com": RegistryAuth(
            login=env.str("IMAGE_PULL_USER"),
            password=env.str("IMAGE_PULL_TOKEN"),
        ),
    },
)

entries = [namespace, reg_secret]
