import values

from kdsl.core.v1 import ObjectMeta
from kdsl.core.v1 import Service, PodSpec, ObjectMeta, ContainerItem
from kdsl.apps.v1 import Deployment
from kdsl.extra import mk_env
from kdsl.recipe import choice
from common import reg_secret_name

name = "frontend"
labels = dict(component=name)


metadata = ObjectMeta(
    name=name,
    namespace=values.NAMESPACE,
    labels=labels,
)


service = Service(
    metadata=metadata,
    spec=dict(
        selector=labels,
        ports={
            3000: dict(name="http"),
        },
    ),
)


pod_spec = PodSpec(
    imagePullSecrets=[
        dict(name=reg_secret_name),
    ],
    containers=dict(
        main=ContainerItem(
            image=choice(
                dev=values.FRONTEND_DEV_IMAGE,
                prod=values.FRONTEND_PROD_IMAGE,
            ),
            imagePullPolicy="Always",
            ports={
                3000: dict(name="http", protocol="TCP"),
            },
        ),
    ),
)


deployment = Deployment(
    metadata=metadata,
    spec=dict(
        replicas=1,
        selector=dict(matchLabels=labels),
        template=dict(
            metadata=ObjectMeta(labels=labels),
            spec=pod_spec,
        ),
    ),
)


entries = [service, deployment]
