import common
import backend_component
import frontend_component
import proxy_component

from kdsl.utils import render_to_stdout


entries = [
    *common.entries,
    *backend_component.entries,
    *frontend_component.entries,
    *proxy_component.entries,
]


if __name__ == "__main__":
    render_to_stdout(entries)
