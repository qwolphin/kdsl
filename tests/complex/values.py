from envparse import env  # type: ignore

NAMESPACE: str = env.str("NAMESPACE")
DOMAIN: str = env.str("DOMAIN")
BACKEND_IMAGE: str = env.str("BACKEND_IMAGE")
FRONTEND_DEV_IMAGE: str = env.str("FRONTEND_DEV_IMAGE")
FRONTEND_PROD_IMAGE: str = env.str("FRONTEND_PROD_IMAGE")
PROXY_IMAGE: str = env.str("PROXY_IMAGE")
