#!/usr/bin/env bash
set -e

if [[ ! "${KDSL_VERSION?}" =~ ([0-9]{1,3}\.){2}[0-9](\.post[0-9]{1,9})? ]]
then
    echo "${KDSL_VERSION?} is not a valid package version"
    exit 1
fi

export OPENAPI_VERSION=$(echo "${KDSL_VERSION?}" | sed 's/\.post.*//g')
export OPENAPI_PATH="/openapi/${OPENAPI_VERSION?}.json"
export PACKAGE_NAME=kdsl
export JSONPATCH_PATH=/app/data/patch.yaml
export OUTPUT_DIR=/app/generated/kdsl
export DOCS_DATA_DIR=/app/docs/_data


K8S_REPO='https://raw.githubusercontent.com/kubernetes/kubernetes'

if [ ! -f "${OPENAPI_PATH?}" ]
then
    echo "No OpenAPI file present, downloading for ${OPENAPI_VERSION?}"
    wget "${K8S_REPO?}/v${OPENAPI_VERSION?}/api/openapi-spec/swagger.json" \
    --no-verbose -O "${OPENAPI_PATH?}"
fi

cd /app
mkdir -p generated/ docs/_data/
rm -r generated/* || true
rm -r docs/_data/* || true
cp -R -n template/* generated/
traceback-with-variables main.py

echo 'Rendering OK'

if [ -n "$1" ]
then
    chown -R 1000 .
    echo 'First arg is not empty, assuming --no-install' 
    exit 0
fi

echo 'Running install'

cd generated/
python3 setup.py install 1>/dev/null
chown -R 1000 .

echo 'Install OK'
